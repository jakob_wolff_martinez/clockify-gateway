import os

from google.cloud import bigquery
from google.cloud.exceptions import NotFound

from utils.gcp import get_client, create_table, run_query
from utils.utils import get_duration_in_seconds


TOKEN = '778slow32fishgeneralpostclothe9023'

SUPPORTED_EVENTS = [
	'timer_stopped',
	'entry_deleted',
	'entry_updated',
	'entry_created'
]

CLOCKIFY_TABLE_SCHEMA = [
	bigquery.SchemaField('id', 'STRING', mode='REQUIRED'), # entry ID
	bigquery.SchemaField('description', 'STRING', mode='NULLABLE'), # description
	bigquery.SchemaField('user_id', 'STRING', mode='REQUIRED'),
	bigquery.SchemaField('user_name', 'STRING', mode='NULLABLE'),
	bigquery.SchemaField('user_status', 'STRING', mode='NULLABLE'),
	bigquery.SchemaField('billable', 'BOOLEAN', mode='REQUIRED'),
	bigquery.SchemaField('project_id', 'STRING', mode='NULLABLE'),
	bigquery.SchemaField('project', 'STRING', mode='NULLABLE'),
	bigquery.SchemaField('time_start', 'TIMESTAMP', mode='REQUIRED'),
	bigquery.SchemaField('time_end', 'TIMESTAMP', mode='REQUIRED'),
	bigquery.SchemaField('duration', 'DECIMAL', mode='NULLABLE', description='Duration in seconds'),
	bigquery.SchemaField('is_locked', 'BOOLEAN', mode='NULLABLE'),
	bigquery.SchemaField('hourly_rate', 'DECIMAL', mode='NULLABLE'),
	bigquery.SchemaField('cost_rate', 'DECIMAL', mode='NULLABLE'),
]

def import_clockify_data(request):
	provided_token = request.args.get('token')
	if provided_token != TOKEN:
		return ({'message': 'Unauthorized'}, 401)
	event = request.args.get('event')
	if event not in SUPPORTED_EVENTS:
		return ({'message': 'event received is not supported. Must be one of {}'.format(', '.join(SUPPORTED_EVENTS))})
	bq_client = get_client('big_query')
	project = os.environ.get('BQ_PROJECT_NAME', None)
	dataset = os.environ.get('BQ_ETL_DATASET_NAME', None)
	table_name = os.environ.get('TIME_ENTRY_TABLE', None)

	time_entry_table = '{}.{}.{}'.format(
		project,
		dataset,
		table_name
	)

	try:
		bq_client.get_table(time_entry_table)
	except NotFound:
		print('here we goooo')
		create_table(bq_client, time_entry_table, CLOCKIFY_TABLE_SCHEMA)
	
	data = request.get_json()
	user = data.get('user')
	time = data.get('timeInterval')

	sql_data = {
		'id': data['id'],
		'description': data['description'] or 'NULL',
		'user_id': user['id'],
		'user_name': user['name'],
		'user_status': user['status'],
		'billable': data['billable'],
		'project_id': data['projectId'] or 'NULL',
		'project': data['project'] or 'NULL',
		'time_start': time['start'],
		'time_end': time['end'],
		'duration': get_duration_in_seconds(
			time['start'],
			time['end']
		),
		'is_locked': data['isLocked'],
		'hourly_rate': data['hourlyRate'] or 'NULL',
		'cost_rate': data['costRate'] or 'NULL'
	}

	for entry in sql_data:
		if sql_data[entry] != 'NULL' and type(sql_data[entry]) in [str]:
			sql_data[entry] = '\"' + sql_data[entry] + '\"'


	if event in ['timer_stopped', 'entry_created']:
		sql = """
		INSERT INTO {} (id, description, user_id, user_name, user_status, billable, project_id, project, time_start, time_end, duration, is_locked, hourly_rate, cost_rate)
		VALUES ({id}, {description}, {user_id}, {user_name}, {user_status}, {billable}, {project_id}, {project}, {time_start}, {time_end}, {duration}, {is_locked}, {hourly_rate}, {cost_rate});
		""".format(time_entry_table, **sql_data)

	if event == 'entry_deleted':
		sql = """
		DELETE FROM {}
		WHERE id = {}
		""".format(time_entry_table, sql_data['id'])

	if event == 'entry_updated':
		sql = """
			UPDATE {} SET
				description = {description},
				user_id = {user_id},
				user_name = {user_name},
				user_status = {user_status},
				billable = {billable},
				project_id = {project_id},
				project = {project},
				time_start = {time_start},
				time_end = {time_end},
				duration = {duration},
				is_locked = {is_locked},
				hourly_rate = {hourly_rate},
				cost_rate = {cost_rate}
			WHERE id = {id}
		""".format(time_entry_table, **sql_data)
	run_query(bq_client, sql)
	return ({'message': 'success'}, 200)



# 2 flows
# dayly data



# Test cases
# 1. Manual entry
# 2. Tracking via extension
# 3. Update entry
# 4. Delete entry
