from datetime import datetime

def get_duration_in_seconds(start, end):
	start = datetime.strptime(start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime(end, '%Y-%m-%dT%H:%M:%SZ')
	sec = (end-start).total_seconds()
	return sec