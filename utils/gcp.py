import os
import json

from google.cloud import bigquery
from google.oauth2 import service_account

def get_client(service):
	env_var = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS', None)
	if env_var == None:
		logging.error('Env var GOOGLE_APPLICATION_CREDENTIALS not found'.format())
	service_account_info = json.loads(env_var)
	credentials = service_account.Credentials.from_service_account_info(service_account_info)
	if service == 'big_query':
		return bigquery.Client(credentials=credentials, project=credentials.project_id)

	return False

def run_query(bq_client, sql):
	bq_client.query(sql).result()
	return True

def create_table(bq_client, table_id, schema):
	table = bigquery.Table(table_id, schema=schema)
	table = bq_client.create_table(table)
	return True